import React, { Component } from "react";
import LogWrapper from "./LogWrapper";
import { Icon } from "antd-v3";
import { Link, Redirect } from "react-router-dom";
import { LoginConsumer } from "./LoginContext/Index";
import DropDown from "./DropDown";
import Strategic from "./Strategic";

function ComponentA(props) {
  return (
    <LogWrapper module="Component A" button="Button A">
      {props.children}
    </LogWrapper>
  );
}
function ComponentB(props) {
  return (
    <LogWrapper module="Component B" button="Button B">
      {props.children}
      {/* <button onClick={()=>console.log("ComA")}>Demo</button> */}
    </LogWrapper>
  );
}

function ComponentC(props) {
  return (
    <LogWrapper module="Component C" button="Button C">
      {props.children}
    </LogWrapper>
  );
}

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      strategic: false,
    };
  }

  displayStrategic = () => {
    this.setState({
      strategic: true,
    });
  };
  render() {
    return (
      <div>
        <LoginConsumer>
          {(props) => {
            const { authenticated, handleClear } = props;
            if (authenticated) {
              return (
                <div>
                  <div className="header">
                    <Link to="/AboutUs">
                      <button
                        className="logout"
                        onClick={() => {
                          localStorage.setItem(
                            "loginDetails",
                            JSON.stringify({})
                          );
                          handleClear();
                        }}
                      >
                        <Icon type="logout" /> Logout
                      </button>
                    </Link>
                  </div>
                  <div className="content">
                    <h1>Welcome to Dashboard</h1>
                  </div>
                </div>
              );
            } else {
              return <Redirect to="/AboutUs" />;
            }
          }}
        </LoginConsumer>
        <ComponentA>
          <ComponentB>
            <ComponentC></ComponentC>
          </ComponentB>
        </ComponentA>
        <div>
          <DropDown onChange={this.displayStrategic} />
          {this.state.strategic && <Strategic />}
        </div>
      </div>
    );
  }
}

export default Dashboard;
