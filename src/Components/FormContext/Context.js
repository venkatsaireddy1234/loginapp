import React from "react";

const FormContext = React.createContext({
  audienceCategory: "",
  audeinceName: "",
  bussinessPriority: "",
  size: "",
  strategicImperative: "",
  currentBehaviour: "",
  driversandBarriers: "",
  desiredImpact: "",
});

const FormConsumer = FormContext.Consumer

class FormHandling extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      audienceCategory: "",
      audeinceName: "",
      bussinessPriority: "",
      size: "",
      strategicImperative: "",
      currentBehaviour: "",
      driversandBarriers: "",
      desiredImpact: "",
    };
  }
  formOnTarget = (values, oldValues) => {
    console.log("Success:", values, oldValues);
    this.setState({
      audienceCategory: oldValues.audienceCategory,
      audeinceName: oldValues.audeinceName,
      bussinessPriority: oldValues.bussinessPriority,
      size: oldValues.size,
    });
  };

  formOnStrategic = (values, oldValues) => {
    console.log(values, oldValues);
    this.setState({
      strategicImperative: oldValues.strategicImperative,
    });
  };
  formOnBehaviour = (values, oldValues) => {
    console.log("Success:", values, oldValues);
    this.setState({
      currentBehaviour: oldValues.currentBehaviour,
      driversandBarriers: oldValues.driversandBarriers,
      desiredImpact: oldValues.desiredImpact,
    });
  };
  handleClick = () => {
    console.log(this.state);
  };
  render() {
      console.log(this.state);
    return (
      <>
        <FormContext.Provider
          value={{
            audienceCategory: this.state.audienceCategory,
            audeinceName: this.state.audeinceName,
            bussinessPriority: this.state.bussinessPriority,
            size: this.state.size,
            strategicImperative: this.state.strategicImperative,
            currentBehaviour: this.state.currentBehaviour,
            driversandBarriers: this.state.driversandBarriers,
            desiredImpact: this.state.desiredImpact,
            formOnTarget: this.formOnTarget,
            formOnStrategic: this.formOnStrategic,
            formOnBehaviour: this.formOnBehaviour,
            handleClick: this.handleClick,
          }}
        >
          {this.props.children}
        </FormContext.Provider>
      </>
    );
  }
}

export default FormHandling;
export {FormConsumer, FormContext}