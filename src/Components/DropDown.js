import React, { Component } from "react";
import "antd/dist/antd.css";
// import './index.css';
import { Button, Form, Select } from "antd";
import "../App.css";

class DropDown extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  render() {
    console.log(this.props);
    return (
      <Form
        name="basic"
        labelCol={{
          span: 20,
        }}
        wrapperCol={{
          span: 15,
          offset: 5,
        }}
        onFinish={() => this.props.onChange()}
        onFinishFailed={this.onFinishFailed}
        autoComplete="off"
      >
        {" "}
        <div className="dropDown">
          <Form.Item
            label="Select Country"
            name="Country"
            rules={[
              {
                required: true,
                message: "Please enter Country Name!",
               
              },
              
            ]}
          >
            <Select  placeholder="select one">
              <Select.Option value="India">India</Select.Option>
              <Select.Option value="America">America</Select.Option>
              <Select.Option value="Sweden">Sweden</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="Select Category"
            name="category"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Select placeholder="select one">
              <Select.Option value="Coltina">Coltina</Select.Option>
              <Select.Option value="Colina">Colina</Select.Option>
              <Select.Option value="Coltna">Coltna</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </div>
      </Form>
    );
  }
}

export default DropDown;
