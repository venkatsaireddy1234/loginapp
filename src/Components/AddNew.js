import React, { Component } from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Button, Form, Input } from "antd";

const { TextArea } = Input;

class AddNew extends Component {
  state = {};
  onFinish = (values) => {
    console.log("Received values of form:", values);
  };
  render() {
    return (
      <>
        <Form
          name="dynamic_form_nest_item"
          onFinish={this.onFinish}
          autoComplete="off"
          labelCol={{
            span: 20,
            offset: -5,
          }}
          wrapperCol={{
            span: 15,
            offset: 7,
          }}
        >
          <Form.List name="users">
            {(fields, { add, remove }) => (
              <>
                {fields.map(({ key, name, ...restField }) => (
                  <div>
                    <div>
                      <div className="behavorialAnalysiscontent">
                        <Form.Item
                          {...restField}
                          label="Current Behaviour"
                          name="currentBehaviour"
                          rules={[
                            {
                              required: true,
                              message: "Please Enter Current Behaviour",
                            },
                          ]}
                          hasFeedback
                        >
                          <TextArea
                            rows={5}
                            style={{ width: 250 }}
                            placeholder="Enter Current Behaviour"
                          />
                        </Form.Item>
                        <Form.Item
                          {...restField}
                          label="Drivers and Barriers"
                          name="driversandBarriers"
                          rules={[
                            {
                              required: true,
                              message: "Please Enter Drivers and Barriers",
                            },
                          ]}
                          hasFeedback
                        >
                          <TextArea
                            rows={5}
                            style={{ width: 250 }}
                            placeholder="Enter Drivers and Barriers"
                            value={this.state.driversandBarriers}
                          />
                        </Form.Item>
                        <Form.Item
                          {...restField}
                          label="Desired Imapct"
                          name="desiredImpact"
                          rules={[
                            {
                              required: true,
                              message: "Please Enter Desired Impact",
                            },
                          ]}
                          hasFeedback
                        >
                          <TextArea
                            rows={5}
                            style={{ width: 250 }}
                            placeholder="Enter Desired Impact"
                            value={this.state.desiredImpact}
                          />
                        </Form.Item>
                        <Form.Item
                          {...restField}
                          label="Strategic Action"
                          name="strategicaction"
                          rules={[
                            {
                              required: true,
                              message: "Please Enter Strategic Action",
                            },
                          ]}
                          hasFeedback
                        >
                          <TextArea
                            rows={5}
                            style={{ width: 250 }}
                            placeholder="Enter Strategic Action"
                            value={this.state.strategicaction}
                          />
                        </Form.Item>
                        <Button
                          type="danger"
                          onClick={() => remove(name)}
                          block
                          //   icon={<MinusCircleOutlined/>}
                        >
                          Delete
                        </Button>
                      </div>
                    </div>
                  </div>
                ))}
                <Form.Item>
                  <Button
                    style={{ color: "skyblue" }}
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Add New
                  </Button>

                  {/* <MinusCircleOutlined onClick={() => remove(name)} /> */}
                </Form.Item>
                <div></div>
              </>
            )}
          </Form.List>
          {/* <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item> */}
        </Form>
      </>
    );
  }
}

export default AddNew;
